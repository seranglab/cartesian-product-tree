#ifndef _APLUSBLAYERORDEREDHEAPGENERATORSTRICT_HPP
#define _APLUSBLAYERORDEREDHEAPGENERATORSTRICT_HPP

#include "CartesianProductLayerOrderedHeapGenerator.hpp"

#include <iostream>
#include <unordered_set>

template <typename T>
class APlusBLayerOrderedHeapGenerator : public CartesianProductLayerOrderedHeapGenerator<T> {
protected:
  struct LayerProductCornerIndex {
    unsigned long i,j;
    bool is_max;

    bool operator <(const LayerProductCornerIndex & rhs) const {
      if (i < rhs.i)
				return true;
      if (i > rhs.i)
				return false;
      if (j < rhs.j)
				return true;
      if (j > rhs.j)
				return false;
			
      if (is_max < rhs.is_max)
				return true;
      return false;
    }
		
    bool operator ==(const LayerProductCornerIndex & rhs) const {
      return i == rhs.i && j == rhs.j && is_max == rhs.is_max;
    }

    friend std::ostream & operator<<(std::ostream & os, const LayerProductCornerIndex & rhs) {
      return (os << "(" << rhs.i << "," << rhs.j << ")," << rhs.is_max);
    }
  };
	
  struct LayerProductCornerIndexHash {
    std::size_t operator() (const LayerProductCornerIndex & lpci) const {
      return ((lpci.i<<32) ^ lpci.j ^ lpci.is_max);
    }
  };
	
  struct LayerProductCorner {
    T val;
    LayerProductCornerIndex index;
		
    bool operator <(const LayerProductCorner & rhs) const {
      if (val < rhs.val)
				return true;
      if (val > rhs.val)
				return false;
      
      return index < rhs.index;
    }

    friend std::ostream & operator<<(std::ostream & os, const LayerProductCorner & rhs) {
      return (os << rhs.val << "," << rhs.index);
    }
  };
	
  PrimitiveVector<LayerProductCorner> _hull_heap;
  unsigned long _num_max_corner_elements_popped;
  unsigned long _all_cumulative_selections_to_date;
	
  PrimitiveVector<T> _values_considered;
	
  LayerProductCorner _min_corner(unsigned long i, unsigned long j) {
    return LayerProductCorner{this->_loh_a->min_in_layer(i)+this->_loh_b->min_in_layer(j), {i,j, false}};
  }
  LayerProductCorner _max_corner(unsigned long i, unsigned long j) {
    return LayerProductCorner{this->_loh_a->max_in_layer(i)+this->_loh_b->max_in_layer(j), {i,j, true}};
  }

  bool _in_bounds(unsigned long i, unsigned long j) {
    return i < this->_loh_a->n_layers_generated() && j < this->_loh_b->n_layers_generated();
  }

  void _extend_axes_as_necessary(unsigned long i, unsigned long j) {
    if (i >= this->_loh_a->n_layers_generated() && this->_loh_a->are_more_layers_available())
      this->_loh_a->compute_next_layer_if_available();
    if (j >= this->_loh_b->n_layers_generated() && this->_loh_b->are_more_layers_available())
      this->_loh_b->compute_next_layer_if_available();
  }

  void _insert_if_in_bounds_and_extend_axes_as_necessary(unsigned long i, unsigned long j, bool is_max) {
    _extend_axes_as_necessary(i,j);

    if (! _in_bounds(i,j))
      return;

    if (is_max)
      _hull_heap.push_back(_max_corner(i,j));
    else
      _hull_heap.push_back(_min_corner(i,j));
    
    #ifdef MAX_LOH
    std::push_heap(_hull_heap.begin(), _hull_heap.end());
    #else
    // Invert < operator because heap defaults to max heap:
    std::push_heap(_hull_heap.begin(), _hull_heap.end(), [](const LayerProductCorner & lhs, const LayerProductCorner & rhs){return !(lhs<rhs);});
    #endif
  }

  virtual void _insert_neighbors_into_heap(unsigned long i, unsigned long j) {
    // Kaplan-like scheme: prevents adding twice and thus eliminates need for _hull_set:
    if (j==0)
      _insert_if_in_bounds_and_extend_axes_as_necessary(i+1, j, false);
    _insert_if_in_bounds_and_extend_axes_as_necessary(i, j+1, false);
    _insert_if_in_bounds_and_extend_axes_as_necessary(i, j, true);
  }

  virtual void _insert_values_considered(unsigned long i, unsigned long j) {
    unsigned long result_index = _values_considered.size();
    _values_considered.resize(_values_considered.size() + this->_loh_a->layer_size(i)*this->_loh_b->layer_size(j));
    T*a_layer = this->_loh_a->layer_begin(i);
    T*b_layer = this->_loh_b->layer_begin(j);
    const unsigned long a_layer_size = this->_loh_a->layer_size(i);
    const unsigned long b_layer_size = this->_loh_b->layer_size(j);
    for (unsigned long ii=0; ii<a_layer_size; ++ii) {
      T a_val = a_layer[ii];
      T*location_in_values_considered = &_values_considered[result_index];
      std::copy(b_layer, b_layer+b_layer_size, location_in_values_considered);
      for (unsigned long jj=0; jj<b_layer_size; ++jj) 
        location_in_values_considered[jj] += a_val;

      result_index += b_layer_size;
    }
  }

  virtual unsigned long _get_layer_product_size(unsigned long i, unsigned long j) const {
    return this->_loh_a->layer_size(i)*this->_loh_b->layer_size(j);
  }

  void _pop_next_layer_product() {
    #ifdef SAFE
    assert(_hull_heap.size() > 0);
    #endif
    #ifdef MAX_LOH
    std::pop_heap(_hull_heap.begin(), _hull_heap.end());
    #else
    // Invert < operator because heap defaults to max heap:
    std::pop_heap(_hull_heap.begin(), _hull_heap.end(), [](const LayerProductCorner & lhs, const LayerProductCorner & rhs){return !(lhs<rhs);});
    #endif
    //LayerProductCorner result = _hull_heap.back();
    long result_i = _hull_heap.back().index.i;
    long result_j = _hull_heap.back().index.j;
    long result_is_max = _hull_heap.back().index.is_max;    
    _hull_heap.pop_back();

    unsigned long layer_product_size = _get_layer_product_size(result_i, result_j);
    if (! result_is_max) {
      _insert_neighbors_into_heap(result_i, result_j);
      // In same layer product tile, min will get popped before max;
      // therefore, only add values from min corners to avoid adding twice.
      _values_considered.reserve_more(layer_product_size);
      _insert_values_considered(result_i, result_j);
    }
    else {
      // Only count elements when max corner is popped:
      _num_max_corner_elements_popped += layer_product_size;
    }
  }

  // Note: k here is the new selection amount (not the cumulative)
  T*_select(unsigned long k) {
    _all_cumulative_selections_to_date += k;
    #ifdef SAFE
    assert( _all_cumulative_selections_to_date <= this->n_elements_possible() );
    #endif

    while (_num_max_corner_elements_popped < _all_cumulative_selections_to_date)
      _pop_next_layer_product();

    #ifdef SAFE
    assert(_values_considered.size() >= k);
    #endif    
    unsigned long num_unused_vals = _values_considered.size() - k;

    #ifdef MAX_LOH
    std::nth_element(_values_considered.begin(), _values_considered.begin()+num_unused_vals-1, _values_considered.end(), [](T lhs, T rhs){return lhs<rhs;});
    #else
    std::nth_element(_values_considered.begin(), _values_considered.begin()+num_unused_vals-1, _values_considered.end(), [](T lhs, T rhs){return lhs>rhs;});
    #endif
    
    T*result = new T[k];
    std::copy(_values_considered.begin()+num_unused_vals, _values_considered.end(), result);
    _values_considered.resize(num_unused_vals);
    return result;
  }
public:
  APlusBLayerOrderedHeapGenerator(LayerOrderedHeapGenerator<T>*loh_a, LayerOrderedHeapGenerator<T>*loh_b, LayerArithmetic*la):
    CartesianProductLayerOrderedHeapGenerator<T>(loh_a, loh_b, la)
  {
    _insert_if_in_bounds_and_extend_axes_as_necessary(0,0,false);
    _all_cumulative_selections_to_date = 0;
    _num_max_corner_elements_popped = 0;
  }

  void compute_next_layer_if_available() override {
    if (this->are_more_layers_available()) {
      unsigned long new_layer_size = this->next_layer_size();
      T*new_layer = this->_select(new_layer_size);
      this->add_layer(new_layer, new_layer_size);
    }
  }

};

#endif
