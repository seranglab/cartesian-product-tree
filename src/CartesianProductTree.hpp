#ifndef __CARTESIAN_PRODUCT_TREE_HPP
#define __CARTESIAN_PRODUCT_TREE_HPP

#include <numeric>
#include <cstring>

#include "ArrayLayerOrderedHeapGenerator.hpp"
#include "LayerOrderedHeapGenerator.hpp"
   
#ifdef WOBBLY
#include "APlusBLayerOrderedHeapGeneratorWobbly.hpp"
#else
#include "APlusBLayerOrderedHeapGeneratorStrict.hpp"
#endif

template <typename T>
class CartesianProductTree{
private:
  T** _vectors;
  long* _vector_sizes;  
  LayerOrderedHeapGenerator<T>* _root;
  LayerArithmetic* _la;
  long _m;
  long _n;
  double _alpha;

  void _build_tree(){
    std::vector<LayerOrderedHeapGenerator<T>* > current_layer;
    for (long i=0; i < _m; ++i) {
      ArrayLayerOrderedHeapGenerator<T>* leaf = new ArrayLayerOrderedHeapGenerator<T>(_vectors[i], _vector_sizes[i], _la);
      current_layer.push_back(leaf);
    }

    while (current_layer.size() > 1){
      std::vector<LayerOrderedHeapGenerator<T>* > next_layer;
      for (unsigned long i=0; i < (current_layer.size()>>1); ++i)
				next_layer.emplace_back(new APlusBLayerOrderedHeapGenerator<T>(current_layer[2*i], current_layer[2*i + 1], _la));
      if (current_layer.size()%2 == 1)
				next_layer.push_back(current_layer[current_layer.size() - 1]);
      current_layer = next_layer;
    }
		_root = current_layer[0];
  }
	
public:
  CartesianProductTree(T** input_vectors, long* input_vector_sizes, LayerArithmetic* la, long n, long m, double alpha):
    _vectors(input_vectors),
    _vector_sizes(input_vector_sizes),
    _la(la),
    _m(m),
    _n(n),
    _alpha(alpha)
  {
    _build_tree();
  }
	
  T* select(unsigned long k){
    while (_root->n_elements_generated() < k) {
      _root->compute_next_layer_if_available();
    }
		
    T* result = new T[_root->n_elements_generated()];
    long result_index = 0;
    for (unsigned layer = 0; layer < _root->n_layers_generated(); ++layer){
      //fixme: use memcpy to copy each layer (for performance)
      for (long i = 0; i < (_root->layer_end(layer) - _root->layer_begin(layer)); ++i){
				result[result_index] = *(_root->layer_begin(layer)+i);
				++result_index;
      }
    }
    //std::cout << "n_elements_generated() : " << _root->n_elements_generated() << std::endl;
    std::nth_element(result, result+k-1, result + _root->n_elements_generated());
    
    return result;
  }
	
};

#endif
