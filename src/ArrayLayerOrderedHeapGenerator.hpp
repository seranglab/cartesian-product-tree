#ifndef _ARRAYLAYERORDEREDHEAPGENERATOR_HPP
#define _ARRAYLAYERORDEREDHEAPGENERATOR_HPP

#include "LayerOrderedHeapGenerator.hpp"
#include "LayerOrderedHeap.hpp"

template <typename T>
class ArrayLayerOrderedHeapGenerator : public LayerOrderedHeapGenerator<T>{
private:
  LayerOrderedHeap<T> _loh;

public:
  ArrayLayerOrderedHeapGenerator(T*data, long n, LayerArithmetic*la):
    LayerOrderedHeapGenerator<T>(la),
    _loh(data, n, la)
  {
    this->_number_of_elements_possible = n;
    this->_number_of_elements_generated = 0;
  }

  T*layer_begin(unsigned long layer_i) const override {
    return _loh.layer_begin(layer_i);
  }

  T*layer_end(unsigned long layer_i) const override {
    return _loh.layer_end(layer_i);
  }

  void compute_next_layer_if_available() override {
    if (this->are_more_layers_available()){
      this->_number_of_elements_generated += this->layer_size(this->_number_of_layers_generated);
      ++this->_number_of_layers_generated;
    }
  }

};

#endif
