#ifndef _DYNAMICLAYERORDEREDHEAPGENERATOR_HPP
#define _DYNAMICLAYERORDEREDHEAPGENERATOR_HPP

#include "LayerOrderedHeapGenerator.hpp"

// To be used by everything but ArrayLayerOrderedHeapGenerator:
template <typename T>
class DynamicLayerOrderedHeapGenerator : public LayerOrderedHeapGenerator<T> {
protected:
  PrimitiveVector<T*> _layers;
  PrimitiveVector<unsigned long> _layer_sizes;

  void add_layer(T*new_layer, unsigned long new_layer_size) {
    // Put min and max elements at start and end of layer:
    T*layer_end = new_layer + new_layer_size;

    T* min_ptr = new_layer;
    T* max_ptr = new_layer;
    T* i = new_layer;
    ++i;

    for (; i < layer_end; ++i) {
      if (*min_ptr > *i)
        min_ptr = i;
      else if (*max_ptr < *i)
        max_ptr = i;
    }

    if (max_ptr == new_layer) {
      std::swap(*new_layer, *min_ptr);
      std::swap(*(layer_end-1), *min_ptr);
    }
    else if (max_ptr  == min_ptr) {
      std::swap(*new_layer, *min_ptr);
      std::swap(*(layer_end-1), *new_layer);
    }
    else {
      std::swap(*new_layer, *min_ptr);
      std::swap(*(layer_end-1), *max_ptr);
    }

    _layers.push_back(new_layer);
    _layer_sizes.push_back(new_layer_size);

    ++this->_number_of_layers_generated;
    this->_number_of_elements_generated += new_layer_size;
  }
public:
  DynamicLayerOrderedHeapGenerator(LayerArithmetic*la):
    LayerOrderedHeapGenerator<T>(la)
  {}
  
  ~DynamicLayerOrderedHeapGenerator() {
    for (unsigned long i=0; i<_layer_sizes.size(); ++i)
      // Layers should be allocated with malloc
      free( _layers[i] );
  }
  
  T*layer_begin(unsigned long layer_i) const override {
    return _layers[layer_i];
  }
  
  T*layer_end(unsigned long layer_i) const override {
    return _layers[layer_i] + _layer_sizes[layer_i];
  }
};

#endif
