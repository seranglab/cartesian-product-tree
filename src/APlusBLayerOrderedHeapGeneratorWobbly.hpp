#ifndef _APLUSBLAYERORDEREDHEAPGENERATORWOBBLY_HPP
#define _APLUSBLAYERORDEREDHEAPGENERATORWOBBLY_HPP

#include "CartesianProductLayerOrderedHeapGenerator.hpp"

#include <iostream>
#include <unordered_set>

template <typename T>
class APlusBLayerOrderedHeapGenerator : public CartesianProductLayerOrderedHeapGenerator<T> {
protected:
  struct LayerProductCornerIndex {
    unsigned long i,j;
    bool is_max;
    
    bool operator <(const LayerProductCornerIndex & rhs) const {
      if (i < rhs.i)
        return true;
      if (i > rhs.i)
        return false;
      if (j < rhs.j)
        return true;
      if (j > rhs.j)
        return false;
      
      return is_max < rhs.is_max;
    }
    
    bool operator ==(const LayerProductCornerIndex & rhs) const {
      return i == rhs.i && j == rhs.j && is_max == rhs.is_max;
    }
    
    friend std::ostream & operator<<(std::ostream & os, const LayerProductCornerIndex & rhs) {
      return (os << "(" << rhs.i << "," << rhs.j << ")," << rhs.is_max);
    }
  };
  
  struct LayerProductCornerIndexHash {
    std::size_t operator() (const LayerProductCornerIndex & lpci) const {
      return ((lpci.i<<32) ^ lpci.j ^ lpci.is_max);
    }
  };
  
  struct LayerProductCorner {
    T val;
    LayerProductCornerIndex index;
    
    bool operator <(const LayerProductCorner & rhs) const {
      if (val < rhs.val)
        return true;
      if (val > rhs.val)
        return false;
      
      return index < rhs.index;
    }
    
    friend std::ostream & operator<<(std::ostream & os, const LayerProductCorner & rhs) {
      return (os << rhs.val << "," << rhs.index);
    }
  };
  
  PrimitiveVector<LayerProductCorner> _hull_heap;
  unsigned long _max_corner_area_popped;
  
  PrimitiveVector<T> _values_certain;
  PrimitiveVector<T> _purgatory;
  
  LayerProductCorner _min_corner(unsigned long i, unsigned long j) {
    return LayerProductCorner{this->_loh_a->min_in_layer(i)+this->_loh_b->min_in_layer(j), {i,j, false}};
  }
  LayerProductCorner _max_corner(unsigned long i, unsigned long j) {
    return LayerProductCorner{this->_loh_a->max_in_layer(i)+this->_loh_b->max_in_layer(j), {i,j, true}};
  }
  
  bool _in_bounds(unsigned long i, unsigned long j) {
    return i < this->_loh_a->n_layers_generated() && j < this->_loh_b->n_layers_generated();
  }
  
  void _extend_axes_as_necessary(unsigned long i, unsigned long j) {
    if (i >= this->_loh_a->n_layers_generated() && this->_loh_a->are_more_layers_available())
      this->_loh_a->compute_next_layer_if_available();
    if (j >= this->_loh_b->n_layers_generated() && this->_loh_b->are_more_layers_available())
      this->_loh_b->compute_next_layer_if_available();
  }
  
  void _insert_if_in_bounds_and_extend_axes_as_necessary(unsigned long i, unsigned long j, bool is_max) {
    _extend_axes_as_necessary(i,j);
    
    if (! _in_bounds(i,j))
      return;
    
    if (is_max)
      _hull_heap.push_back(_max_corner(i,j));
    else {
      _hull_heap.push_back(_min_corner(i,j));
    }
    
    #ifdef MAX_LOH
    std::push_heap(_hull_heap.begin(), _hull_heap.end());
    #else
    // Invert < operator because heap defaults to max heap:
    std::push_heap(_hull_heap.begin(), _hull_heap.end(), [](const LayerProductCorner & lhs, const LayerProductCorner & rhs){return !(lhs<rhs);});
    #endif
  }
  
  void _insert_neighbors_into_heap(unsigned long i, unsigned long j) {
    // Kaplan-like scheme: prevents adding twice and thus eliminates need for _hull_set:
    if (j==0)
      _insert_if_in_bounds_and_extend_axes_as_necessary(i+1, j, false);
    _insert_if_in_bounds_and_extend_axes_as_necessary(i, j+1, false);
    _insert_if_in_bounds_and_extend_axes_as_necessary(i, j, true);
  }
  
  void _insert_values(PrimitiveVector<T> & res, unsigned long i, unsigned long j) {
    unsigned long result_index = res.size();
    res.resize(res.size() + this->_get_layer_product_size(i,j));
    T*a_layer = this->_loh_a->layer_begin(i);
    T*b_layer = this->_loh_b->layer_begin(j);
    const unsigned long a_layer_size = this->_loh_a->layer_size(i);
    const unsigned long b_layer_size = this->_loh_b->layer_size(j);
    
    for (unsigned long ii=0; ii<a_layer_size; ++ii) {
      T a_val = a_layer[ii];
      T*location_inres = &res[result_index];

      std::copy(b_layer, b_layer+b_layer_size, location_inres);
      for (unsigned long jj=0; jj<b_layer_size; ++jj) 
        location_inres[jj] += a_val;

      result_index += b_layer_size;
    }
  }
  
  unsigned long _get_layer_product_size(unsigned long i, unsigned long j) const {
    return this->_loh_a->layer_size(i)*this->_loh_b->layer_size(j);
  }
  
  void _pop_next_layer_product() {
    #ifdef SAFE
    assert(_hull_heap.size() > 0);
    #endif
    #ifdef MAX_LOH
    std::pop_heap(_hull_heap.begin(), _hull_heap.end());
    #else
    // Invert < operator because heap defaults to max heap:
    std::pop_heap(_hull_heap.begin(), _hull_heap.end(), [](const LayerProductCorner & lhs, const LayerProductCorner & rhs){return !(lhs<rhs);});
    #endif
    long result_i = _hull_heap.back().index.i;
    long result_j = _hull_heap.back().index.j;
    long result_is_max = _hull_heap.back().index.is_max;    

    //LayerProductCorner result = _hull_heap.back();
    _hull_heap.pop_back();
    
    if (! result_is_max)
      _insert_neighbors_into_heap(result_i, result_j);
    else{
      // max corner:
      _insert_values(_values_certain, result_i, result_j);
      
      unsigned long layer_product_size = _get_layer_product_size(result_i, result_j);
      _max_corner_area_popped += layer_product_size;
    }
  }
  
  // Note: k here is the new selection amount (not the cumulative)
  std::pair<T*, unsigned long> _get_next_at_least_k_or_everything_remaining(unsigned long k) {
    // Must dump purgatory into values certain if heap is empty
    if (_hull_heap.size()==0) {
      std::pair<T*, unsigned long> data_and_size = _purgatory.liberate_pointer();
      return data_and_size;
    }

    _max_corner_area_popped = 0;
    while (_max_corner_area_popped < k && _hull_heap.size()>0)
      _pop_next_layer_product();
    
    // go through hull and get all max corners (which came from min
    // corners that were popped)
    PrimitiveVector<LayerProductCorner> min_corners_to_remain_in_hull;
    for (const LayerProductCorner & lpc : _hull_heap) {
      if (lpc.index.is_max)
        _insert_values(_purgatory, lpc.index.i, lpc.index.j);
      else
        min_corners_to_remain_in_hull.push_back(lpc);
    }
    _hull_heap = std::move(min_corners_to_remain_in_hull);
    
    #ifdef MAX_LOH
    std::make_heap(_hull_heap.begin(), _hull_heap.end());
    #else
    std::make_heap(_hull_heap.begin(), _hull_heap.end(), [](auto lhs, auto rhs){return !(lhs<rhs);});
    #endif
    
    T worst_max_corner_certain = *std::max_element(_values_certain.begin(), _values_certain.end());
    
    #ifdef MAX_LOH
    auto iter = std::partition(_purgatory.begin(), _purgatory.end(), [worst_max_corner_certain](auto lhs){return lhs<worst_max_corner_certain;});
    #else
    // invert comparison to put good things on right, bad things on left:
    auto iter = std::partition(_purgatory.begin(), _purgatory.end(), [worst_max_corner_certain](auto lhs){return lhs>worst_max_corner_certain;});
    #endif
    
    // take good values from _purgatory and push them into
    // _values_certain. then resize _purgatory to only include bad
    // values:
    
    // iter refers to first good thing
    unsigned long num_bad_elements_in_purgatory = iter - _purgatory.begin();
    unsigned long num_good_elements_in_purgatory = _purgatory.size() - num_bad_elements_in_purgatory;
    unsigned long i=_values_certain.size();
    
    _values_certain.resize(_values_certain.size() + num_good_elements_in_purgatory);
    for (; iter < _purgatory.end(); ++iter, ++i)
      _values_certain[i] = *iter;
    _purgatory.resize(num_bad_elements_in_purgatory);

    // liberate that pointer!
    // viva la mort
    // viva la guerre
    // viva la sacré libérer
    std::pair<T*, unsigned long> data_and_size = _values_certain.liberate_pointer();
    return data_and_size;
  }
  
public:
  APlusBLayerOrderedHeapGenerator(LayerOrderedHeapGenerator<T>*loh_a, LayerOrderedHeapGenerator<T>*loh_b, LayerArithmetic*la):
    CartesianProductLayerOrderedHeapGenerator<T>(loh_a, loh_b, la)
  {
    _insert_if_in_bounds_and_extend_axes_as_necessary(0,0,false);
  }
  
  void compute_next_layer_if_available() override {
    if (this->are_more_layers_available()) {
      unsigned long new_layer_size_lower_bound = this->next_layer_size();
      std::pair<T*, unsigned long> new_layer = this->_get_next_at_least_k_or_everything_remaining(new_layer_size_lower_bound);
      this->add_layer(new_layer.first, new_layer.second);
    }
  }

};

#endif
