#ifndef _CARTESIANPRODUCTLAYERORDEREDHEAPGENERATOR_HPP
#define _CARTESIANPRODUCTLAYERORDEREDHEAPGENERATOR_HPP

#include "DynamicLayerOrderedHeapGenerator.hpp"

template <typename T>
class CartesianProductLayerOrderedHeapGenerator : public DynamicLayerOrderedHeapGenerator<T>{
protected:
  LayerOrderedHeapGenerator<T>*_loh_a;
  LayerOrderedHeapGenerator<T>*_loh_b;
  
public:
  CartesianProductLayerOrderedHeapGenerator(LayerOrderedHeapGenerator<T> *loh_a, LayerOrderedHeapGenerator<T> *loh_b, LayerArithmetic*la):
    DynamicLayerOrderedHeapGenerator<T>(la),
    _loh_a(loh_a),
    _loh_b(loh_b)
  { 
    this->_number_of_elements_possible = _loh_a->n_elements_possible()*_loh_b->n_elements_possible();
  }
};

#endif
