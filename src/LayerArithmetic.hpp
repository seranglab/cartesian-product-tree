#ifndef _LAYERARITHMETIC_HPP
#define _LAYERARITHMETIC_HPP

#include "PrimitiveVector.hpp"

#include <math.h>
#include <algorithm>

class LayerArithmetic {
private:
  double _alpha;
  PrimitiveVector<unsigned long> _partition_indices;
  unsigned long _n_supported;

  double _power_of_alpha;
  double _partition_index;

  void _add_partition_index_for_next_layer() {
    _partition_indices.push_back( (unsigned long)(ceil(_partition_index)) );

    _power_of_alpha *= _alpha;
    _partition_index += _power_of_alpha;

    _n_supported = _partition_indices.back()+1;
  }

public:
  LayerArithmetic(double alpha):
    _alpha(alpha),
    _n_supported(0),
    _power_of_alpha(1.),
    _partition_index(1.)
  {
    // Start with 0, even though it isn't a partition index; this
    // allows the looking up layer starts and ends without if
    // statements.
    _partition_indices.push_back(0);
    #ifdef SAFE
    assert(alpha>=1.);
    #endif
  }

  LayerArithmetic(double alpha, unsigned long n_elements_to_support):
    LayerArithmetic(alpha)
  {
    unsigned long number_to_support = std::max(10ul, n_elements_to_support+1);
    guarantee_n_elements_supported(number_to_support);
    // Supports n elements, but no more:
    if (n_elements_to_support <= _partition_indices.back()+1)
      _partition_indices.pop_back();
  }

  unsigned long get_partition_index(unsigned long partition_i) const {
    #ifdef SAFE
    assert(partition_i < _partition_indices.size());
    #endif
    return _partition_indices[partition_i]; 
  }


  unsigned long get_flat_layer_start_index(long partition_i) const {
    // Do not shift partition here, because already shifted in
    // get_partition_index
    return get_partition_index(partition_i);
  }

  unsigned long get_flat_layer_end_index(long partition_i) const {
    // Do not shift partition here, because already shifted in
    // get_partition_index
    return get_partition_index(partition_i+1);
  }

  unsigned long layer_size(long partition_i) const {
    return get_flat_layer_end_index(partition_i) - get_flat_layer_start_index(partition_i);
  }

  unsigned long number_of_layers_supported() const {
    // subtract 1 because of shift by +1 in get_flat_layer_end_index
    // (see 0 pushed in constructor)

    // subtract another 1 because we are accessing via [] operator
    // (which starts at 0)
    return _partition_indices.size()-2;
  }

  void guarantee_i_layers_supported(unsigned long i) {
    while (number_of_layers_supported() < i)
      _add_partition_index_for_next_layer();
  }

  void guarantee_n_elements_supported(unsigned long n) {
    while (_n_supported <= n)
      _add_partition_index_for_next_layer();
  }

  double alpha() const {
    return _alpha;
  }

  const unsigned long*begin() const {
    return (_partition_indices.begin()+1);
  }
  const unsigned long*end() const {
    return _partition_indices.end();
  }
  unsigned long n_partitions() const {
    return _partition_indices.size()-1;
  }
};

#endif
