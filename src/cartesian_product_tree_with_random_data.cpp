#include <vector>
#include <assert.h>
#include <tuple>
#include <algorithm>

#include "CartesianProductTree.hpp"
#include "Clock.hpp"
#include "PrimitiveVector.hpp"
#include "io.hpp"

int* brute_force_m_is_3(int** vectors, long n, long k) {
	PrimitiveVector<int> results;
	for (long i=0; i<n; ++i)
		for (long j=0; j<n; ++j)
			for (long k=0; k<n; ++k)
				results.push_back(vectors[0][i] + vectors[1][j] + vectors[2][k]);
	std::sort(results.begin(), results.end());
	int* top_k = new int[k];
	for (long i=0; i<k; ++i)
		top_k[i] = results[i];
	return top_k;
}

int* brute_force_m_is_2(int** vectors, long n, long k) {
	PrimitiveVector<int> results;
	for (long i=0; i<n; ++i)
		for (long j=0; j<n; ++j)
			results.push_back(vectors[0][i] + vectors[1][j]);
	std::sort(results.begin(), results.end());
	int* top_k = new int[k];
	for (long i=0; i<k; ++i)
		top_k[i] = results[i];
	return top_k;
}


int main(int argc, char**argv){
  if (argc != 6 )
    std::cerr << "usage:  <n> <m> <k> <alpha> <seed>" << std::endl;
  else {
    // Gather parameters		
    long n = read<long>(argv[1]);
    long m = read<long>(argv[2]);
    long k = read<long>(argv[3]);
    double alpha = read<double>(argv[4]);
    long seed = read<long>(argv[5]);
    srand(seed);
    long max_int = 10000;

    int** vector_ptrs = new int*[m];
    long* vector_sizes = new long[m];
    for (long i=0; i < m; ++i) {
      vector_ptrs[i] = new int[n];
      vector_sizes[i] = n;
      for (long j=0; j < n; ++j) {
				int val = rand()%(max_int+1);
				vector_ptrs[i][j] = val;
      }
    }
		
    // Initialize objects
    LayerArithmetic* la = new LayerArithmetic(alpha);
		
    Clock c;
    CartesianProductTree<int> tree(vector_ptrs, vector_sizes, la, n, m, alpha);
    int* results = tree.select(k);
		printn("Results:", str(results, results+k));
    c.ptock();

		int* bf_results;

		if (m==2 || m==3) {
			if (m==2)
				bf_results = brute_force_m_is_2(vector_ptrs, n, k);
			else
				bf_results = brute_force_m_is_3(vector_ptrs, n, k);

			std::sort(results, results+k);
				
		  for (long i=0; i<k; ++i)
				assert(bf_results[i] == results[i]);
			printn("PASSED");
		}

  }
  return 0;
}
